jQuery(document).ready(function(){
    
    function reCalculate(event){
        var viewPortWidth = document.body.scrollWidth;
        jQuery('.owl-carousel').find('.owl-item').each(function(index,element){ 
            var slideWidth = jQuery(this).width();
            var activeSlideCaption = jQuery(this).find('figcaption');
            var padLeft = 30;
            if ( jQuery(activeSlideCaption).css('padding-left') != '' ) padLeft = parseInt(jQuery(activeSlideCaption).css('padding-left'))+10;
            var p70 = Math.round(((viewPortWidth - slideWidth)/2)*0.7);
            var captionWidth = Math.round((viewPortWidth - slideWidth)/2) - padLeft;
            if ( captionWidth > p70 ) captionWidth = p70;
            jQuery(activeSlideCaption).width(captionWidth);  
        })
    }
    
    function reCalculateMobile(event){
        var topMargin = jQuery('#vlog-responsive-header').height(); 
        var galHeight = Math.round((jQuery( window ).height() - topMargin)/2);
        jQuery('.owl-carousel').find('.gallery-icon.portrait img').each(function(i,e){
            jQuery(this).css( 'max-height', galHeight+'px' );
        })
    }
    
    /* Full height gallery */

    jQuery( window ).load(function() {
        var topMargin = 0;
        if (document.body.clientWidth > 991 ) { 
            topMargin = jQuery('.vlog-top-bar').height()+jQuery('#header').height(); 
        } else {
            topMargin = jQuery('#vlog-responsive-header').height(); 
        };
        var galHeight = jQuery( window ).height() - topMargin;
        if (document.body.clientWidth > 1024) {
            jQuery('.vlog-single-full-height-gallery-cover').height( galHeight );
            
                var owl = jQuery('.vlog-single-full-height-gallery-cover .gallery').owlCarousel({
                center: true,
                loop:true,
                margin:0,
                nav: true,
                autoWidth: true,
                navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                onInitialized: reCalculate
                });
            
        }
        else if (document.body.clientWidth == 1024 ) {
            jQuery('.vlog-single-full-height-gallery-cover  .gallery').height( galHeight - Math.round((jQuery( window ).height() - topMargin)/3) );  
                var owl = jQuery('.vlog-single-full-height-gallery-cover .gallery').owlCarousel({
                    center: true,
                    loop:true,
                    margin:0,
                    nav: true,
                    autoWidth: true,
                    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                    onInitialized: reCalculate
                });
        }        
        else {  
                var owl = jQuery('.vlog-single-full-height-gallery-cover .gallery').owlCarousel({
                    center: false,
                    items: 1,
                    loop:true,
                    margin:0,
                    nav: true,
                    autoWidth: false,
                    autoHeight: false,
                    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                    onInitialized: reCalculateMobile
                });        
        }
        
    });
    

})