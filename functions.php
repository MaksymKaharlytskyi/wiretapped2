<?php

/* Define Theme Vars */
define( 'VLOG_THEME_VERSION', '1.3' );

/* Define content width */
if ( !isset( $content_width ) ) {
	$content_width = 1104;
}

/* Localization */
load_theme_textdomain( 'vlog', get_template_directory()  . '/languages' );

/* After theme setup main hook */
add_action( 'after_setup_theme', 'vlog_theme_setup' );

/**
 * After Theme Setup
 *
 * Callback for after_theme_setup hook
 *
 * @return void
 * @since  1.0
 */

function vlog_theme_setup() {

	/* Add thumbnails support */
	add_theme_support( 'post-thumbnails' );

	/* Add theme support for title tag (since wp 4.1) */
	add_theme_support( 'title-tag' );


	/* Add image sizes */
	$image_sizes = vlog_get_image_sizes();

	if ( !empty( $image_sizes ) ) {
		foreach ( $image_sizes as $id => $size ) {
			add_image_size( $id, $size['args']['w'], $size['args']['h'], $size['args']['crop'] );
		}
	}

	/* Add post formats support */
	add_theme_support( 'post-formats', array(
			'audio', 'gallery', 'image', 'video'
		) );

	/* Support for HTML5 */
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

	/* Automatic Feed Links */
	add_theme_support( 'automatic-feed-links' );

}

/* Helpers and utility functions */
include_once ( get_template_directory() . '/core/helpers.php' );

/* Load frontend scripts */
include_once ( get_template_directory() . '/core/enqueue.php' );

// /* Module-specific functions */
include_once ( get_template_directory() . '/core/modules.php' );

/* Template functions */
include_once ( get_template_directory() . '/core/template-functions.php' );

/* Menus */
include_once ( get_template_directory() . '/core/menus.php' );

/* Sidebars */
include_once ( get_template_directory() . '/core/sidebars.php' );

/* Widgets */
include_once ( get_template_directory() . '/core/widgets.php' );

/* Extensions (hooks and filters to add/modify specific features ) */
include_once ( get_template_directory() . '/core/extensions.php' );

/* Add support for our mega menu */
include_once ( get_template_directory() . '/core/mega-menu.php' );

if ( is_admin() ) {

/* Admin helpers and utility functions  */
include_once ( get_template_directory() . '/core/admin/helpers.php' );

/* Load admin scripts */
include_once ( get_template_directory() . '/core/admin/enqueue.php' );

/* Theme Options */
include_once ( get_template_directory() . '/core/admin/options.php' );

/* Demo importer panel */
include_once ( get_template_directory() . '/core/admin/demo-importer.php' );

/* Include plugins - TGM Init */
include_once ( get_template_directory() . '/core/admin/plugins.php' );

/* Include AJAX action handlers */
include_once ( get_template_directory() . '/core/admin/ajax.php' );

/* Extensions ( hooks and filters to add/modify specific features ) */
include_once ( get_template_directory() . '/core/admin/extensions.php' );

/* Custom metaboxes */
include_once ( get_template_directory() . '/core/admin/metaboxes.php' );

}


add_action('template_include', 'load_single_template');
function load_single_template($template) {
  $new_template = '';
 
  // single post template
  if( is_single() ) {
    global $post;
 
    // template for post with video format
    if ( has_post_format( 'video' )) {
      // use template file single-video.php for video format
      $new_template = locate_template(array('single-video.php' ));
    }
 
  }
  return ('' != $new_template) ? $new_template : $template;
}


add_filter( 'body_class','my_body_classes' );
function my_body_classes( $classes ) {
 
    if ( is_page('live') ) {
     
        $classes[] = 'live-boxed';
         
    }
     
    return $classes;
     
}

function excerpt($limit) {
      $excerpt = explode(' ', get_the_excerpt(), $limit);
      if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...';
      } else {
        $excerpt = implode(" ",$excerpt);
      } 
      $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
      return $excerpt;
    }

    function content($limit) {
      $content = explode(' ', get_the_content(), $limit);
      if (count($content)>=$limit) {
        array_pop($content);
        $content = implode(" ",$content).'...';
      } else {
        $content = implode(" ",$content);
      } 
      $content = preg_replace('/\[.+\]/','', $content);
      $content = apply_filters('the_content', $content); 
      $content = str_replace(']]>', ']]&gt;', $content);
      return $content;
    }


function widget_categories_args_filter( $cat_args ) {
$exclude_arr = array( 23,33 );

if( isset( $cat_args['exclude'] ) && !empty( $cat_args['exclude'] ) )
    $exclude_arr = array_unique( array_merge( explode( ',', $cat_args['exclude'] ), $exclude_arr ) );
$cat_args['exclude'] = implode( ',', $exclude_arr );
return $cat_args;
}

add_filter( 'widget_categories_args', 'widget_categories_args_filter', 10, 1 );

function the_category_filter($thelist,$separator=' ') {
     // list the IDs of the categories to exclude
     $exclude = array(23,33);
     // create an empty array
     $exclude2 = array();

     // loop through the excluded IDs and get their actual names
     foreach($exclude as $c) {
          // store the names in the second array
          $exclude2[] = get_cat_name($c);
     }

     // get the list of categories for the current post     
     $cats = explode($separator,$thelist);
     // create another empty array      
     $newlist = array();

     foreach($cats as $cat) {
          // remove the tags from each category
          $catname = trim(strip_tags($cat));

          // check against the excluded categories
          if(!in_array($catname,$exclude2))

          // if not in that list, add to the new array
          $newlist[] = $cat;
     }
     // return the new, shortened list
     return implode($separator,$newlist);
}

// add the filter to 'the_category' tag
add_filter('the_category','the_category_filter', 10, 2);

add_filter( 'rwmb_meta_boxes', 'wiretapped_prefix_meta_boxes' );
function wiretapped_prefix_meta_boxes( $meta_boxes ) {
    $prefix = 'wiretapped_';
    $meta_boxes[] = array(
        'title'      => __( 'Full Height Gallery', 'textdomain' ),
        'post_types' => 'post',
        'context'    => 'side',
        'priority'   => 'high',
        'autosave'   => true,
        'fields'     => array(
            array(
                'id'      => "{$prefix}fullgallery",
                'name'    => __( 'Use this post as a full height gallery?', 'textdomain' ),
                'desc'    => 'yes, please',
                'type'    => 'checkbox',
                'std'     => 0,
                )
        ),
    );
    return $meta_boxes;
}

function wp_get_attachment_by_ID( $attachment_id ) {

	$attachment = get_post( $attachment_id );
	return array(
		'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
		'caption' => $attachment->post_excerpt,
		'description' => $attachment->post_content,
		'href' => get_permalink( $attachment->ID ),
		'src' => $attachment->guid,
		'title' => $attachment->post_title
	);
}

remove_shortcode( 'gallery' );
add_shortcode( 'gallery', 'wiretapped_gallery_shortcode' );
function wiretapped_gallery_shortcode( $attr ) {
	$post = get_post();

	static $instance = 0;
	$instance++;

	if ( ! empty( $attr['ids'] ) ) {
		// 'ids' is explicitly ordered, unless you specify otherwise.
		if ( empty( $attr['orderby'] ) ) {
			$attr['orderby'] = 'post__in';
		}
		$attr['include'] = $attr['ids'];
	}

	/**
	 * Filters the default gallery shortcode output.
	 *
	 * If the filtered output isn't empty, it will be used instead of generating
	 * the default gallery template.
	 *
	 * @since 2.5.0
	 * @since 4.2.0 The `$instance` parameter was added.
	 *
	 * @see gallery_shortcode()
	 *
	 * @param string $output   The gallery output. Default empty.
	 * @param array  $attr     Attributes of the gallery shortcode.
	 * @param int    $instance Unique numeric ID of this gallery shortcode instance.
	 */
	$output = apply_filters( 'post_gallery', '', $attr, $instance );
	if ( $output != '' ) {
		return $output;
	}

	$html5 = current_theme_supports( 'html5', 'gallery' );
	$atts = shortcode_atts( array(
		'order'      => 'ASC',
		'orderby'    => 'menu_order ID',
		'id'         => $post ? $post->ID : 0,
		'itemtag'    => $html5 ? 'figure'     : 'dl',
		'icontag'    => $html5 ? 'div'        : 'dt',
		'captiontag' => $html5 ? 'figcaption' : 'dd',
		'columns'    => 3,
		'size'       => 'thumbnail',
		'include'    => '',
		'exclude'    => '',
		'link'       => ''
	), $attr, 'gallery' );

	$id = intval( $atts['id'] );
    $post_title = get_the_title($id);

	if ( ! empty( $atts['include'] ) ) {
		$_attachments = get_posts( array( 'include' => $atts['include'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );

		$attachments = array();
		foreach ( $_attachments as $key => $val ) {
			$attachments[$val->ID] = $_attachments[$key];
		}
	} elseif ( ! empty( $atts['exclude'] ) ) {
		$attachments = get_children( array( 'post_parent' => $id, 'exclude' => $atts['exclude'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
	} else {
		$attachments = get_children( array( 'post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
	}

	if ( empty( $attachments ) ) {
		return '';
	}

	if ( is_feed() ) {
		$output = "\n";
		foreach ( $attachments as $att_id => $attachment ) {
			$output .= wp_get_attachment_link( $att_id, $atts['size'], true ) . "\n";
		}
		return $output;
	}

	$itemtag = tag_escape( $atts['itemtag'] );
	$captiontag = tag_escape( $atts['captiontag'] );
	$icontag = tag_escape( $atts['icontag'] );
	$valid_tags = wp_kses_allowed_html( 'post' );
	if ( ! isset( $valid_tags[ $itemtag ] ) ) {
		$itemtag = 'dl';
	}
	if ( ! isset( $valid_tags[ $captiontag ] ) ) {
		$captiontag = 'dd';
	}
	if ( ! isset( $valid_tags[ $icontag ] ) ) {
		$icontag = 'dt';
	}

	$columns = intval( $atts['columns'] );
	$itemwidth = $columns > 0 ? floor(100/$columns) : 100;
	$float = is_rtl() ? 'right' : 'left';

	$selector = "gallery-{$instance}";

	$gallery_style = '';

	/**
	 * Filters whether to print default gallery styles.
	 *
	 * @since 3.1.0
	 *
	 * @param bool $print Whether to print default gallery styles.
	 *                    Defaults to false if the theme supports HTML5 galleries.
	 *                    Otherwise, defaults to true.
	 */
	if ( apply_filters( 'use_default_gallery_style', ! $html5 ) ) {
		$gallery_style = "
		<style type='text/css'>
			#{$selector} {
				margin: auto;
			}
			#{$selector} .gallery-item {
				float: {$float};
				margin-top: 10px;
				text-align: center;
				width: {$itemwidth}%;
			}
			#{$selector} img {
				border: 2px solid #cfcfcf;
			}
			#{$selector} .gallery-caption {
				margin-left: 0;
			}
			/* see gallery_shortcode() in wp-includes/media.php */
		</style>\n\t\t";
	}

	$size_class = sanitize_html_class( $atts['size'] );
	$gallery_div = "<div id='$selector' class='gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'>";

	/**
	 * Filters the default gallery shortcode CSS styles.
	 *
	 * @since 2.5.0
	 *
	 * @param string $gallery_style Default CSS styles and opening HTML div container
	 *                              for the gallery shortcode output.
	 */
	$output = apply_filters( 'gallery_style', $gallery_style . $gallery_div );

	$i = 0;
	foreach ( $attachments as $id => $attachment ) {
        
        $attachment_metas = wp_get_attachment_by_ID($id);

		$attr = ( trim( $attachment->post_excerpt ) ) ? array( 'aria-describedby' => "$selector-$id" ) : '';
		if ( ! empty( $atts['link'] ) && 'file' === $atts['link'] ) { 
			$image_output = wp_get_attachment_link( $id, $atts['size'], false, false, false, $attr );
		} elseif ( ! empty( $atts['link'] ) && 'none' === $atts['link'] ) {
			$image_output = wp_get_attachment_image( $id, $atts['size'], false, $attr );
		} else {
			$image_output = wp_get_attachment_link( $id, $atts['size'], true, false, false, $attr );
		}
		$image_meta  = wp_get_attachment_metadata( $id );

		$orientation = '';
		if ( isset( $image_meta['height'], $image_meta['width'] ) ) {
			$orientation = ( $image_meta['height'] > $image_meta['width'] ) ? 'portrait' : 'landscape';
		}
		$output .= "<{$itemtag} class='gallery-item'>";
		$output .= "
			<{$icontag} class='gallery-icon {$orientation}'>
				$image_output
			</{$icontag}>";
		if ( $captiontag && trim($attachment->post_excerpt) ) {
			$output .= "
				<{$captiontag} class='wp-caption-text gallery-caption' id='$selector-$id'>
                <h3 class='post_title'>" . $post_title . "</h3>
                <h4 class='title'>" . $attachment_metas['title'] . "</h4>
				<div class='desc'>" . wptexturize($attachment->post_excerpt) . "</div>
				</{$captiontag}>";
		}
		$output .= "</{$itemtag}>";
		if ( ! $html5 && $columns > 0 && ++$i % $columns == 0 ) {
			$output .= '<br style="clear: both" />';
		}
	}

	if ( ! $html5 && $columns > 0 && $i % $columns !== 0 ) {
		$output .= "
			<br style='clear: both' />";
	}

	$output .= "
		</div>\n";

	return $output;
}

?>