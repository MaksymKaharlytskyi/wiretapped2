<?php
/**
 * Template Name: About Page Modules
 */
?>
<?php get_header(); ?>

<style>
.vlog-site-content {
    padding-top: 0px!important;
}
.vlog-site-content .vlog-section:last-child {
    display: none;
}
</style>

<?php $vlog_meta = vlog_get_page_meta( get_the_ID() ); ?>

<?php if( isset( $vlog_meta['fa'] ) && $vlog_meta['fa']['layout'] != 'none' ) : ?>

    <?php $fa = vlog_get_featured_area_query( $vlog_meta['fa'] ); ?>
    <?php include( locate_template('template-parts/cover/area-' . absint( $vlog_meta['fa']['layout'] ) . '.php') ); ?>
    <?php wp_reset_postdata(); ?>

<?php endif; ?>

<?php get_template_part('template-parts/ads/below-header'); ?>

<?php
    
    global $vlog_sidebar_opts;
    $sections = $vlog_meta['sections'];
?>

<?php if ( !empty( $sections ) ) : ?>

    <?php 
        
        //Check if pagination is set and do required tweaks
        if( $vlog_meta['pag'] != 'none' ){
            
            $pagination = $vlog_meta['pag'];
            vlog_set_paginated_module_index( $sections );
            $paged = vlog_module_template_is_paged();
            
            if( $paged ){
                $sections = vlog_parse_paged_module_template( $sections );
                vlog_set_paginated_module_index( $sections, $paged );
            }
        }

    ?>


    <!-- Custom Page Stuff Here -->
  <div class="vlog-section vlog-no-sid " style="
    background-color: #5d2879;
    padding-top: 30px;
">

            <div class="container">
                
                

                <div class="vlog-content">

                    <div class="row">

                        
                                                                
                                   <div class="vlog-module module-text col-lg-12 col-xs-12" id="vlog-module-0-0">
    <div class="vlog-mod-head"><div class="vlog-mod-title"><h4 style="
    color: #ffffff;
">About WireTapped</h4></div></div>
            <div class="vlog-txt-module">
                        <p style="
    color: #fff;
">Wiretapped is the new trusted source on all things pop culture! A dynamic and informative web experience created by award-winning television journalist Sharon Carpenter and entrepreneur Sidac Barriteau, Wiretapped TV delivers credible entertainment news, provocative celebrity interviews, super exclusives and fun interactive features, allowing viewers to engage celebs like never before - and vice versa. "Wiretapped: LIVE", the site's flagship show, utilizes the best interactive video-broadcast technology to deliver live, intimate celebrity news and interviews everywhere to everyone.</p>
<p style="
    color: #fff;
">Follow Wiretapped + Sharon Carpenter<br>
@wiretappedtv<br>
@sharoncarpenter</p>
        </div>
    
</div>
                            
                        
                    </div>

                </div>


                
            </div>

        </div>
        
                
        <div class="vlog-section vlog-no-sid vlog-bg" style="
    background: url(http://wiretapped2.mediaexplode.com/wp/wp-content/uploads/2016/10/Sharon-copy-1.jpg);
    background-size: contain;
    background-repeat: no-repeat;
    margin-bottom: 0px;
">

            <div class="vlog-content" style="
">

                    <div class="row">

                        
                                                                
                                   <div class="vlog-module module-text col-lg-12 col-xs-12" id="vlog-module-1-0" style="
    text-align: right;
    float: right;
    width: 55%;
    padding-right: 150px;
">
    <div class="vlog-mod-head"><div class="vlog-mod-title"><h4>Sharon Carpenter</h4></div></div>
            <div class="vlog-txt-module">
                        <p>Sharon Carpenter is Co-Founder and Co-President of Wiretapped where she oversees editorial content and development.</p>
<p>An award-winning broadcast journalist, TV host and producer, Sharon originally hails from the UK.  Throughout her career she has made tremendous waves as an on-air personality for some of America’s best known networks including CBS, BET, VH1, BBC America and Sean “Diddy” Combs’ REVOLT TV.  Sharon can currently be seen as a guest correspondent on the top-rated TV shows "Entertainment Tonight” and “The Insider.”  Meanwhile, she recently made her acting debut, co-starring as herself on Season 2 of the Fox prime-time smash “Empire.”  Sharon makes regular appearances as a ‘Hot Talk’ panelist on the "Wendy Williams Show." Behind-the-scenes, she works as a television producer and is known for co-creating VH1’s hit docu-series "The Gossip Game."</p>
<p>Sharon has interviewed a diverse range of A-list subjects including Oprah Winfrey, Tom Hanks, Robert Pattinson, Sienna Miller, Harrison Ford, Ryan Gosling, Emma Stone, Will Smith, Carrie Underwood, Beyoncé, Jay-Z, Rihanna, Serena Williams, Senator Cory Booker and many more.</p>
<p>She has a bachelors degree in Business Management from Pace University where she graduated magna cum laude. </p>
        </div>
    
</div>
                            
                        
                    </div>

                </div><div class="container">
                
                

                


                
            </div>

        </div>

        
                
        <div class="vlog-section " style="
    background: url(http://wiretapped2.mediaexplode.com/wp/wp-content/uploads/2016/10/Chris_Profile_300x299.png);
    background-size: contain;
    background-repeat: no-repeat;
    background-position: right;
    background-color: #000000;
    padding-top: 30px;
    margin-bottom: 0px;
">

            <div class="vlog-content" style="
    width: 100%;
    color: #ffffff;
">

                    <div class="row">

                        
                                                                
                                   <div class="vlog-module module-text col-lg-12 col-xs-12" id="vlog-module-2-0" style="
    text-align: left;
    float: left;
    padding-left: 150px;
    width: 55%;
">
    <div class="vlog-mod-head"><div class="vlog-mod-title"><h4 style="
    color: #ff0000;
">Sidac Barriteau</h4></div></div>
            <div class="vlog-txt-module">
                        <p>Sidac Barriteau, is Co-Founder and Co-President of Wiretapped where he oversees strategic development, media and technology.</p>
<p>Sidac has over 20 years’ experience as a business professional and strategist with Iffesta Media, a New York City based media tech consultancy.  Primarily focused on digital and new media entertainment, Sidac leverages his business insight to advise clients worldwide.</p>
<p>Prior to Iffesta Media, Sidac was a Senior Strategist at Zinc International.  In his capacity at Zinc, Sidac worked on new business development, providing strategic advice to senior executives on international infrastructure and development projects.  During that time, he worked on range of assignments in the entertainment, technology, energy, oil, minerals, and real estate.</p>
<p>For almost 15 years Sidac worked on Wall Street, serving as Senior Vice President and Head of Business Development at HedgeCo Securities, and for ten years as a Vice President and institutional bond salesman at Lehman Brothers.</p>
<p>Sidac is a graduate of Howard University.</p>
        </div>
    
</div>
                            
                        
                    </div>

                </div><div class="container">
                
                

                


                                    

    <div class="vlog-sidebar vlog-sidebar-right">

        
        
    </div>

                
            </div>

        </div>
    <!--Custom page Stuff Ends -->

    <?php foreach ( $sections as $s_ind => $section ) : ?>
    
        <?php 
            $vlog_sidebar_opts = $section;
            $section_class = $section['use_sidebar'] == 'none' ? 'vlog-no-sid ' : '';
            $section_class .= $section['bg'];
        ?>
        
        <div class="vlog-section <?php echo esc_attr( $section_class ); ?>">

            <div class="container">
                
                <?php if( $vlog_sidebar_opts['use_sidebar'] == 'left' ): ?>
                    <?php get_template_part('sidebar'); ?>
                <?php endif; ?>


                <div class="vlog-content">

                    <div class="row">

                        <?php if(!empty($section['modules'])): ?>

                            <?php foreach( $section['modules'] as $m_ind => $module ): $module = vlog_parse_args( $module, vlog_get_module_defaults( $module['type'] ) ); ?>
                                    
                                   <?php include( locate_template('template-parts/modules/'.$module['type'].'.php') ); ?>

                            <?php endforeach; ?>

                        <?php endif; ?>

                    </div>

                </div>


                <?php if( $vlog_sidebar_opts['use_sidebar'] == 'right' ): ?>
                    <?php get_template_part('sidebar'); ?>
                <?php endif; ?>

            </div>

        </div>

    <?php endforeach; ?>

<?php else: ?>

    <div class="vlog-section">

        <div class="container">

            <div class="vlog-content">

                <?php

                    $args = array(
                        'title' => '<h4>'. esc_html__( 'Oooops!', 'vlog' ).'</h2>',
                        'desc' =>  wp_kses( sprintf( __( 'You don\'t have any sections and modules yet. Hurry up and <a href="%s">create your first module</a>.', 'vlog' ), admin_url( 'post.php?post='.get_the_ID().'&action=edit#vlog_modules' ) ), wp_kses_allowed_html( 'post' ))
                    );

                    echo vlog_module_heading( $args );
                ?>

            </div>

        </div>

    </div>

<?php endif; ?>

<?php get_footer(); ?>