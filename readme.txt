== Change Log ==

= 1.3 =

* Added: "X" button in watch later area to for easier posts removal
* Added: Wacth later menu in mobile navigation
* Added: Option to load watch later area asynchronously and prevent conflicts with caching plugins (Theme Options -> Misc.)
* Added: Option to display full post content instead of excerpt in Layout A (Theme Options -> Post Layouts)
* Added: More social media icons for header social menu (500px.com, amazon.com, ok.ru, mixcloud.com)
* Improved: Social sharing buttons not working in some specific cases (i.e. having UTF8 characters in post titles)
* Improved: Mobile navigation
* Improved: Theme rendering performances
* Fixed: RTL styles and few minor styling issues
* Fixed: Featured area 1 minor styling issues

= 1.2 =
* Added: Support for Facebook Videos
* Added: Support for JW Player Platform videos
* Fixed: RTL styles and few minor styling issues
* Improved previous/next navigation of single posts on mobile devices


= 1.1 =
* Added: 2 new demo examples in Theme Options -> Demo Importer
* Fixed: Few minor styling bugs

= 1.0 =
* Initial release