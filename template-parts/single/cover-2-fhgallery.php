<div class=" vlog-featured vlog-single-cover vlog-single-full-height-gallery-cover">

    <?php remove_filter( 'shortcode_atts_gallery', 'vlog_gallery_atts', 10, 3 ); ?>
    <?php if ( $gallery = hybrid_media_grabber( array( 'type' => 'gallery', 'split_media' => true ) ) ): ?>
            <?php echo $gallery; ?>
    <?php endif; ?>
    <?php add_filter( 'shortcode_atts_gallery', 'vlog_gallery_atts', 10, 3 ); ?>

</div>

