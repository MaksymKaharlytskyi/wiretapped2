<?php
$logo = 'http://wiretapped2.mediaexplode.com/wp/wp-content/uploads/2016/08/Wiretapped_New-Logo.png';

if (is_page('live')) {
    $logo = 'http://wiretapped2.mediaexplode.com/wp/wp-content/uploads/2016/10/New-Wiretapped-Live-e1476206316525.png';

} 


$brand = !empty( $logo ) ? '<img class="vlog-logo" src="'.esc_url( $logo ).'" alt="'.esc_attr( get_bloginfo( 'name' ) ).'" >' : get_bloginfo( 'name' );
$logo_only_class = !empty( $logo ) && !vlog_get_option( 'header_site_desc' ) ? 'vlog-logo-only' : '';
?>

<div class="vlog-site-branding <?php echo esc_attr( $logo_only_class ); ?>">

	<?php if ( is_front_page() ) : ?>
		<h1 class="site-title h1"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo $brand; ?></a></h1>
	<?php else : ?>
		<span class="site-title h1"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo $brand; ?></a></span>
	<?php endif; ?>

	<?php if ( vlog_get_option( 'header_site_desc' ) ): ?>
		<?php get_template_part( 'template-parts/header/elements/site-desc' ); ?>
	<?php endif; ?>
</div>
