<?php get_header(); ?>


<?php while ( have_posts() ) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php $single_cover = vlog_get_single_layout(); ?>
		<?php if( $single_cover != 'none'): ?>
			<div class="vlog-featured vlog-featured-1 vlog-single-cover">

		<div class="vlog-featured-item">

			<?php $format = vlog_get_post_format(); ?>
			
			<div class="vlog-cover-bg <?php echo esc_attr($format); ?>">
				
				<?php get_template_part( 'template-parts/formats/' . $format . '-cover' ); ?>
				
				<?php get_template_part( 'template-parts/single/prev-next-cover'); ?>
				
			</div>

			<?php if( ($format != 'video') || ( $format == 'video' && !vlog_get_option('open_videos_inplay') ) ) : ?>

				<div class="vlog-featured-info container vlog-cover-hover-mode vlog-f-hide">

					<div class="row">
							
							<div class="col-lg-12">

								<div class="vlog-featured-info-bg vlog-highlight">
						
									<div class="entry-header">

						                 <?php if( vlog_get_option( 'single_cat' ) ) : ?>
		                    				<span class="entry-category"><?php echo vlog_get_category(); ?></span>
		                				<?php endif; ?>

						                <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						                
							          <?php if( $meta = vlog_get_meta_data( 'single' ) ) : ?>
		                				<div class="entry-meta"><?php echo $meta; ?></div>
		            				  <?php endif; ?>



						             </div>

						             <?php if( $actions = vlog_get_meta_actions( 'single' ) ) : ?>
							             <div class="entry-actions vlog-vcenter-actions">
							             	<?php echo $actions; ?>
							             </div>
							         <?php endif; ?>

					             </div>

					        </div>

					</div>

				</div>

			<?php endif; ?>

			<?php get_template_part( 'template-parts/single/cover-inplay'); ?>

		</div>

		

</div>
		<?php endif; ?>

		<?php get_template_part('template-parts/ads/below-header'); ?>

		<?php global $vlog_sidebar_opts; ?>
		<?php $section_class = $vlog_sidebar_opts['use_sidebar'] == 'none' ? 'vlog-single-no-sid' : '' ?>

		<div class="vlog-section <?php echo esc_attr( $section_class ); ?>">

			<div class="container">

					<?php if( $vlog_sidebar_opts['use_sidebar'] == 'left' ): ?>
						<?php get_sidebar(); ?>
					<?php endif; ?>

					<div class="vlog-content" style="width: 100%">

						<?php if( $single_cover == 'none'): ?>
							<?php get_template_part( 'template-parts/single/classic'); ?>
						<?php endif; ?>
						
						

						<?php get_template_part('template-parts/ads/below-single'); ?>

						<?php get_template_part( 'template-parts/single/prev-next'); ?>
						
						<?php echo do_shortcode('[fbcomments url="" count="off" num="3" countmsg="Discuss This!"]'); ?>

						
							<?php get_template_part( 'template-parts/single/author'); ?>
						

						<?php if( vlog_get_option('single_related') ) : ?>
							<?php $related = vlog_get_related_posts(); ?>

<?php if( $related->have_posts() ) : ?>

	<div id="vlog-related" class="vlog-related-wrapper">	
		<div class="row">
		    <div class="vlog-module module-posts col-lg-12">
		        
		        <?php echo vlog_module_heading(array('title' => '<h4>'.__vlog('related').'</h4>')); ?>

			    <div class="row vlog-posts row-eq-height">
			    	<?php while ( $related->have_posts() ) : $related->the_post(); ?>
			     		<article <?php post_class('vlog-lay-e vlog-post col-lg-4  col-sm-4 col-md-4  col-xs-12'); ?>>
	
	<?php if( $fimg = vlog_get_featured_image('vlog-lay-c') ) : ?>
    <div class="entry-image">
    <a href="<?php echo esc_url( get_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>">
       	<?php echo $fimg; ?>
       	<?php if( vlog_get_option('lay_e_format_label') ) : ?>
        	<?php echo vlog_post_format_icon( 'small' ); ?>
        <?php endif; ?>
    </a>
    </div>
	<?php endif; ?>

	<div class="entry-header">

	    <?php if( vlog_get_option( 'lay_e_cat' ) ) : ?>
	        <span class="entry-category"><?php echo vlog_get_category(); ?></span>
	    <?php endif; ?>

	    <?php the_title( sprintf( '<h2 class="entry-title h5"><a href="%s">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

	</div>
	    
	<?php if( $meta = vlog_get_meta_data( 'e' ) ) : ?>
	    <div class="entry-meta"><?php echo $meta; ?></div>
	<?php endif; ?>


	<?php if( vlog_get_option('lay_e_excerpt') ) : ?>
	    <div class="entry-content">
	        <?php echo vlog_get_excerpt( 'e' ); ?>
	    </div>
	<?php endif; ?>
    

</article>
			     	<?php endwhile; ?>
			    </div>

			</div>
		</div>
	</div>

<?php endif; ?>

<?php wp_reset_postdata(); ?>
						<?php endif; ?>

						

					</div>

					

			</div>

		</div>

	</article>

<?php endwhile; ?>

<?php get_footer(); ?>